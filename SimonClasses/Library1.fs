﻿namespace SimonClasses

open System
open System.Windows
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Diagnostics
open System.Threading
open SimonUserControls

type State = Console | UserInput | Started | HandsReset
type stateMsg =
    | Lock of State
    | Unlock of State
    | Fetch of AsyncReplyChannel<bool>*State

#nowarn "40"
type StateManager() =
    let mutable consoleInUse = false
    let mutable allowUserInput = false
    let mutable started = false
    let mutable handsReset = true
    let mailbox =
        MailboxProcessor.Start(fun inbox ->
            let rec loop =
                async { let! msg = inbox.Receive()
                        match msg with
                        | Lock s -> 
                            match s with
                            | Console -> consoleInUse <- true
                            | UserInput -> allowUserInput <- false
                            | Started -> started <- false
                            | HandsReset -> handsReset <- false
                        | Unlock s ->
                            match s with
                            | Console -> consoleInUse <- false
                            | UserInput -> allowUserInput <- true
                            | Started -> started <- true
                            | HandsReset -> handsReset <- true
                        | Fetch (reply,s) ->
                            match s with
                            | Console -> reply.Reply(consoleInUse)
                            | UserInput -> reply.Reply(allowUserInput)
                            | Started -> reply.Reply(started)
                            | HandsReset -> reply.Reply(handsReset)
                        return! loop
                        }
            loop)
    member this.ConsoleInUse() =
        mailbox.PostAndReply(fun reply -> Fetch(reply, Console))
    member this.UserInputAllowed() =
        mailbox.PostAndReply(fun reply -> Fetch(reply, UserInput))
    member this.GameHasStarted() =
        mailbox.PostAndReply(fun reply -> Fetch(reply, Started))
    member this.HandsReset() =
        mailbox.PostAndReply(fun reply -> Fetch(reply, HandsReset))
    member this.Lock (s : State) =
        mailbox.Post(Lock s)
    member this.Unlock (s : State) =
        mailbox.Post(Unlock s)
                            

type Hand = Left | Right | None

// Class to store data pertaining to a certain button
type Button(gb : GameButton, 
            func : GameButton -> Color -> Color -> int -> int -> Button list -> Controls.TextBlock -> unit, 
            autoFunc : GameButton -> Color -> Color -> int -> int -> unit, 
            on : Color,
            off : Color, 
            pulse : int, 
            tone : int,
            txt : Controls.TextBlock) =
    let gameButton = gb
    let buttonFunction = func
    let automatedButtonFunction = autoFunc
    let onColor = on
    let offColor = off
    let pulseTime = pulse
    let sound = tone
    let buttonId = id
    let mutable buttons : Button list = []
    let textControl = txt
    let mutable handPushedBy = None
    member this.ButtonId = gameButton.ButtonId
    member this.GameButton = gameButton
    member this.SetButtonList buttonList =
        buttons <- buttonList
    member this.Press(h : Hand) = 
        buttonFunction gameButton onColor offColor pulseTime sound buttons textControl
    member this.AutomatedPush() =
        automatedButtonFunction gameButton onColor offColor pulseTime sound