﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SimonUserControls
{
    public static class SoundPlayer
    {
        static System.Media.SoundPlayer player = new System.Media.SoundPlayer(Properties.Resources.Applause);
        
        public static void PlayApplause() 
        {
            player.Play();
            Thread.Sleep(2000);
            player.Stop();
        }
    }
}
