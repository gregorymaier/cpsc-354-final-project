﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimonUserControls
{
    /// <summary>
    /// Interaction logic for GameButton.xaml
    /// </summary>
    public partial class GameButton : UserControl
    {
        public GameButton()
        {
            InitializeComponent();
        }

        #region Dependency properties

        /// <summary>Dependency property to Get/Set Color when IsActive is true</summary>
        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(Color), typeof(GameButton),
                new PropertyMetadata((Color)Colors.Blue, new PropertyChangedCallback(GameButton.OnFillPropertyChanged)));

        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(double), typeof(GameButton),
                new PropertyMetadata(50.0, new PropertyChangedCallback(GameButton.OnRadiusPropertyChanged)));

        public static readonly DependencyProperty LeftTopProperty =
            DependencyProperty.Register("LeftTop", typeof(Point), typeof(GameButton),
                new PropertyMetadata(new Point(0.0, 0.0), new PropertyChangedCallback(GameButton.OnLeftTopPropertyChanged)));

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(GameButton),
                new PropertyMetadata(0.0, new PropertyChangedCallback(GameButton.OnAnglePropertyChanged)));


        #endregion

        #region Wrapper Properties

        public Color Fill
        {
            get
            {
                return (Color)GetValue(FillProperty);
            }
            set
            {
                SetValue(FillProperty, value);
            }
        }

        public double Radius
        {
            get
            {
                return (double)GetValue(RadiusProperty);
            }
            set
            {
                SetValue(RadiusProperty, value);
            }
        }

        public Point LeftTop
        {
            get
            {
                return (Point)GetValue(LeftTopProperty);
            }
            set
            {
                SetValue(LeftTopProperty, value);
            }
        }

        public double Angle
        {
            get
            {
                return (double)GetValue(AngleProperty);
            }
            set
            {
                SetValue(AngleProperty, value);
            }
        }

        public Path RegionPath
        {
            get { return Path; }
        }

        private bool _active = false;
        public bool IsActive
        {
            get { return _active; }
            set { _active = value; }
        }

        public int ButtonId { get; set; }

        #endregion

        #region Callbacks

        private static void OnFillPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GameButton button = (GameButton)d;
            button.Path.Fill = new SolidColorBrush((Color)e.NewValue);
        }

        private static void update(GameButton button)
        {
            double r = button.Radius;
            double xT = button.LeftTop.X;
            double yT = button.LeftTop.Y;
            button.ElliGeo1.RadiusX = r;
            button.ElliGeo1.RadiusY = r;
            button.ElliGeo2.RadiusX = r / 2.0;
            button.ElliGeo2.RadiusY = r / 2.0;
            button.ElliGeo1.Center = button.ElliGeo2.Center = new Point(xT + r, yT + r);
            button.RectGeo1.Rect = new Rect(xT + 0, yT + r, r * 2, r);
            button.RectGeo2.Rect = new Rect(xT + r, yT, r, r * 2);
            button.RectGeo3.Rect = new Rect(xT, yT, r, r);
            button.Transform.CenterX = xT + r / 2.0;
            button.Transform.CenterY = yT + r / 2.0;
        }

        private static void OnRadiusPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            update((GameButton)d);
        }

        private static void OnLeftTopPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            update((GameButton)d);
        }

        private static void OnAnglePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GameButton button = (GameButton)d;
            button.Transform.Angle = (double)e.NewValue;
        }


        #endregion
    }
}
