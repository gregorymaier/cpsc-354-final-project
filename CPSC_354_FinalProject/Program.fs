﻿// Learn more about F# at http://fsharp.net
#light

open System
open System.Windows
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Windows.Threading
open Microsoft.Kinect
open System.Diagnostics
open System.Windows.Forms
open System.Threading
open SimonUserControls
open SimonClasses

// Window Data
let width = 1920.0
let height = 1080.0
let radius = 460.0
let center = Point(730.,290.)
let margin = 20.0

// GameButton Data
let greenTone = 330
let greenAngle = 0.0
let greenOn = Color.FromRgb(0uy, 255uy, 0uy)
let greenOff = Color.FromRgb(0uy, 140uy, 0uy)
let greedId = 0

let redTone = 440
let redAngle = 90.0
let redOn = Color.FromRgb(255uy, 0uy, 0uy)
let redOff = Color.FromRgb(140uy, 0uy, 0uy)
let redId = 1

let yellowTone = 278
let yellowAngle = 270.0
let yellowOn = Color.FromRgb(255uy, 255uy, 0uy)
let yellowOff = Color.FromRgb(140uy, 140uy, 0uy)
let yellowId = 2

let blueTone = 660
let blueAngle = 180.0
let blueOn = Color.FromRgb(0uy, 0uy, 255uy)
let blueOff = Color.FromRgb(0uy, 0uy, 140uy)
let blueId = 3

let lightPulse = 200//ms

// Init random number generator and spin it
let rand = System.Random()
for i in 1 .. 10000 do rand.Next() |> ignore

// Game state
let mutable level = 1
let manager = StateManager()

let mutable difficulty = 0

// Order buttons must be pushed
let mutable buttonPatternHistory : int list = [rand.Next()%4]
let mutable buttonPattern : int list = buttonPatternHistory

// MailboxProcessor so threads can communicate with one another in a safe way

// Change cursor from arrow to hand
Cursor.Current <- Cursors.Hand

// return button from list with specified id
let mapNumberToButton (id : int) (buttonList : Button list) : Button =
    // Assume head of list is correct button
    let mutable result = buttonList.Head
    // Set it to correct button if head is not correct
    for i in 1 .. buttonList.Length - 1 do
        if buttonList.Item(i).ButtonId = id then result <- buttonList.Item(i)
    result

// Show player the order the buttons are to be pushed
let playPattern (buttonList : Button list) =
    manager.Lock UserInput
    for i in buttonPattern do
        System.Console.WriteLine(i.ToString())
        let button = mapNumberToButton i buttonList
        // Have to do this in a new thread for light to flash correctly
        async {
            button.GameButton.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> button.AutomatedPush())) |> ignore
        } |> Async.StartImmediate
        // Let Async workflow finish before starting the next one
        Thread.Sleep(500)
    manager.Unlock UserInput

// For starting game/next level
let startGame (buttonList : Button list) =
    async {
        manager.Lock HandsReset
        Thread.Sleep(750)
        playPattern buttonList
        manager.Unlock Console
    } |> Async.Start

let adjustDifficulty step =
    if difficulty < lightPulse then
        difficulty <- difficulty + step

// Game Logic for when button is pushed
let runGameLogic (buttonPushed : GameButton) (buttonList : Button list) (levelLabel : Controls.TextBlock) =
    System.Console.WriteLine(buttonPattern.Head.ToString() + " " + buttonPushed.ButtonId.ToString())
    if buttonPattern.Head = buttonPushed.ButtonId then
        adjustDifficulty 10
        // Player pressed correct button in sequence
        match buttonPattern with
        | hd::tail ->
            match tail with
            // Correctly matched whole pattern
            | [] -> manager.Lock UserInput
                    manager.Lock Console
                    manager.Lock HandsReset
                    SimonUserControls.SoundPlayer.PlayApplause()
                    buttonPatternHistory <- buttonPatternHistory@[rand.Next()%4]
                    // Setup next level
                    buttonPattern <- buttonPatternHistory
                    // Update graphics
                    level <- level + 1
                    if level < 10 then
                        levelLabel.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> levelLabel.Text <- "0" + level.ToString())) |> ignore
                    else
                        levelLabel.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> levelLabel.Text <- level.ToString())) |> ignore
                    // Start next level
                    startGame buttonList
            // Update list of buttons left to push in sequence
            | h::t -> buttonPattern <- tail
        // This will never match
        | _ -> ()
    // Player hit the wrong button
    else
        manager.Lock UserInput
        manager.Lock Console
        manager.Lock HandsReset
        manager.Lock Started
        difficulty <- 0
        level <- 1
        buttonPatternHistory <- [rand.Next()%4]
        buttonPattern <- buttonPatternHistory
        levelLabel.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> levelLabel.Text <- "XX")) |> ignore
        System.Console.Beep(800, 300)
        System.Console.Beep(800, 300)
        System.Console.Beep(400, 1500)
        
// Make sure that if press event gets triggered that the cursor is in correct range -> acutally over a button
let validDistanceAway min max w h fuzzy (p : Drawing.Point) =
    let deltaX = (float)(p.X-w/2)
    let deltaY = (float)(p.Y-h/2)
    let distance = sqrt(deltaX**2.+deltaY**2.)
    distance-fuzzy <= max && distance+fuzzy >= min

// Convert Joint position to Screen position
let getDisplayPosition w h (joint : Joint) =
    let x = ((w * (float)joint.Position.X + 2.0) / 4.0) + (w/2.0)
    let y = ((h * -(float)joint.Position.Y + 2.0) / 4.0) + (h/2.0)
    let scaleFactor = 1.175
    new Point(x*scaleFactor,y*scaleFactor)

// Draw shape at Joint location
let draw (joint : Joint) (sh : System.Windows.Shapes.Shape) =
    let p = getDisplayPosition width height joint
    sh.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> System.Windows.Controls.Canvas.SetLeft(sh, p.X))) |> ignore
    sh.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> System.Windows.Controls.Canvas.SetTop(sh, p.Y))) |> ignore

// simulate having 2 cursors so right/left hand can cause MouseEnter events
let mutable currentHand = Right
// Move cursor to right hand position
let moveMouse (s : Skeleton) =
    let actualPosRh = getDisplayPosition width height (s.Joints.Item(JointType.HandRight))
    let actualPosLh = getDisplayPosition width height (s.Joints.Item(JointType.HandLeft))
    let rhPoint = Drawing.Point((int)actualPosRh.X, (int)actualPosRh.Y)
    let lhPoint = Drawing.Point((int)actualPosLh.X, (int)actualPosLh.Y)
    if not (manager.HandsReset()) && manager.UserInputAllowed() && not (manager.ConsoleInUse()) then
        if (validDistanceAway 0. 260. ((int)width) ((int)height) 0. rhPoint) && (validDistanceAway 0. 260. ((int)width) ((int)height) 0. lhPoint) then
            manager.Unlock HandsReset
    let mutable newCursorPos = Drawing.Point()
    // switch hands to simulate two cursors
    match currentHand with
    | Right -> currentHand <- Left
    | _ -> currentHand <- Right
    if currentHand = Right then
        newCursorPos <- rhPoint
    else
        newCursorPos <- lhPoint
    if (validDistanceAway 270. 500. ((int)width) ((int)height) 50. newCursorPos) || not (manager.GameHasStarted()) then
       Cursor.Position <- newCursorPos
    // Freeze the mouse for 1ms to reduce jitter
    Thread.Sleep(1)

// Function to listen for SkeletonFrameReady events and perform function actions on Skeleton Data
let skeletonProcessor (sensor : KinectSensor) actions =
    let rec loop () =
        async {
            let! args = Async.AwaitEvent sensor.SkeletonFrameReady
            use frame = args.OpenSkeletonFrame()
            let skeletons : Skeleton[] = Array.zeroCreate(frame.SkeletonArrayLength)
            frame.CopySkeletonDataTo(skeletons)
            skeletons 
            |> Seq.filter (fun s -> s.TrackingState <> SkeletonTrackingState.NotTracked)
            |> Seq.iter  (fun s ->  actions s)
            return! loop ()
        }
    loop ()
    
// Simulate pressing button of Simon game
// Switch light color and back
// Play a tone
let onGameButtonPush (b : GameButton) (on : Color) (off : Color) (pulse : int) (tone : int)  (buttonList : Button list) (levelLabel : Controls.TextBlock) =
    let validCall = validDistanceAway 270. 500. ((int)width) ((int)height) 50. Cursor.Position
    if manager.UserInputAllowed() && not (b.IsActive || manager.ConsoleInUse()) && validCall && manager.GameHasStarted() && manager.HandsReset() then
        b.IsActive <- true
        manager.Lock Console
        manager.Lock HandsReset
        b.Fill <- on
        async { System.Console.Beep(tone, 400) } |> Async.Start
        async { 
            // sleep for pulse ms so light appears to flash
            Thread.Sleep(pulse) 
            b.Dispatcher.Invoke(fun _ -> b.Fill <- off)
        } |> Async.Start
        async {
            // Don't allow any other button to be pushed while current is being pushed
            Thread.Sleep(500)
            // Game logic here
            runGameLogic b buttonList levelLabel
            manager.Unlock Console
            b.IsActive <- false
        } |> Async.Start
    else
        System.Console.WriteLine("Hardware Unavailable")

// Same as onGameButtonPush but action is done unconditionally
let onAutomatedGameButtonPush (b : GameButton) (on : Color) (off : Color) (pulse : int) (tone : int) =
    b.Fill <- on
    async { System.Console.Beep(tone, 400-difficulty) } |> Async.Start
    async { 
        Thread.Sleep(pulse) 
        b.Dispatcher.Invoke(fun _ -> b.Fill <- off)
    } |> Async.Start
    async {
        Thread.Sleep(500-difficulty)
        manager.Unlock Console
    } |> Async.Start

// Skeleton stream smoothing options
let mutable smoothing : TransformSmoothParameters = TransformSmoothParameters()
smoothing.Smoothing <- (float32)0.5
smoothing.Correction <- (float32)0.1
smoothing.Prediction <- (float32)0.5
smoothing.JitterRadius <- (float32)0.01
smoothing.MaxDeviationRadius <- (float32)0.01

[<STAThread>]
do
    // Init hardware
    while KinectSensor.KinectSensors.Count < 1 do
        MessageBox.Show("No KinectSensor Available") |> ignore
        
    let sensor = KinectSensor.KinectSensors.[0]
    sensor.SkeletonStream.Enable(smoothing)
    //sensor.SkeletonStream.EnableTrackingInNearRange <- true
    sensor.Start()

    // Set-up the WPF window and its contents
    let w = Window(Width=width, Height=height, Top=0., Left=0., Cursor=Input.Cursors.None, ForceCursor=true)
    let g = Controls.Grid()
    let c = Controls.Canvas(Background=Brushes.Black)
    ignore <| g.Children.Add c

    // Background of game hardware
    let gameBase = Shapes.Ellipse(Width=1035., Height=1035., Fill=Media.SolidColorBrush(Color.FromRgb(30uy,30uy,30uy)))
    ignore <| c.Children.Add gameBase
    System.Windows.Controls.Canvas.SetLeft(gameBase, width/2.-gameBase.Width/2.)
    System.Windows.Controls.Canvas.SetTop(gameBase, 5.)

    // Middle of the Game
    let silverPlate = Shapes.Ellipse(Width=400., Height=400., Fill=Media.SolidColorBrush(Colors.Silver))
    ignore <| c.Children.Add silverPlate
    System.Windows.Controls.Canvas.SetLeft(silverPlate, width/2.-silverPlate.Width/2.)
    System.Windows.Controls.Canvas.SetTop(silverPlate, 525.-silverPlate.Height/2.)

    let nameLabel = Controls.TextBlock(Text="   Simon", Height=125., Width=350., FontSize=100., FontFamily=FontFamily("Impact"))
    ignore <| c.Children.Add nameLabel
    System.Windows.Controls.Canvas.SetLeft(nameLabel, width/2.-nameLabel.Width/2.-10.)
    System.Windows.Controls.Canvas.SetTop(nameLabel, 515.-nameLabel.Height/2.-60.)

    let levelLabel = Controls.TextBlock(Text="00", Height=125., Width=350., FontSize=120., FontFamily=FontFamily("Digital-7 Mono"))
    ignore <| c.Children.Add levelLabel
    System.Windows.Controls.Canvas.SetLeft(levelLabel, width/2.-levelLabel.Width/2.+125.)
    System.Windows.Controls.Canvas.SetTop(levelLabel, 615.-levelLabel.Height/2.+25.)

    // Setup the 4 buttons
    let greenGameButton = GameButton(Radius=radius, 
                                              Fill=greenOff, 
                                              LeftTop=Point(center.X-radius/2.-margin, center.Y-radius/2.-margin),
                                              Angle=greenAngle,
                                              ButtonId=greedId)
    let greenButton = Button(greenGameButton, onGameButtonPush, onAutomatedGameButtonPush, greenOn, greenOff, lightPulse, greenTone, levelLabel)
    ignore <| c.Children.Add greenGameButton
    greenGameButton.MouseEnter.Add(fun _ -> if Cursor.Position.X < (int)(width/2.) then greenButton.Press(currentHand))
    
    let redGameButton = GameButton(Radius=radius, 
                                              Fill=redOff, 
                                              LeftTop=Point(center.X+radius/2.+margin, center.Y-radius/2.-margin),
                                              Angle=redAngle,
                                              ButtonId=redId)
    let redButton = Button(redGameButton, onGameButtonPush, onAutomatedGameButtonPush, redOn, redOff, lightPulse, redTone, levelLabel)
    ignore <| c.Children.Add redGameButton
    redGameButton.MouseEnter.Add(fun _ -> if Cursor.Position.X > (int)(width/2.) then redButton.Press(currentHand))

    let yellowGameButton = GameButton(Radius=radius, 
                                              Fill=yellowOff, 
                                              LeftTop=Point(center.X-radius/2.-margin, center.Y+radius/2.+margin),
                                              Angle=yellowAngle,
                                              ButtonId=yellowId)
    let yellowButton = Button(yellowGameButton, onGameButtonPush, onAutomatedGameButtonPush, yellowOn, yellowOff, lightPulse, yellowTone, levelLabel)
    ignore <| c.Children.Add yellowGameButton
    yellowGameButton.MouseEnter.Add(fun _ -> if Cursor.Position.X < (int)(width/2.) then yellowButton.Press(currentHand))

    let blueGameButton = GameButton(Radius=radius, 
                                              Fill=blueOff, 
                                              LeftTop=Point(center.X+radius/2.+margin, center.Y+radius/2.+margin),
                                              Angle=blueAngle,
                                              ButtonId=blueId)
    let blueButton = Button(blueGameButton, onGameButtonPush, onAutomatedGameButtonPush, blueOn, blueOff, lightPulse, blueTone, levelLabel)
    ignore <| c.Children.Add blueGameButton
    blueGameButton.MouseEnter.Add(fun _ -> if Cursor.Position.X > (int)(width/2.) then blueButton.Press(currentHand))

    let buttons = [blueButton; redButton; yellowButton; greenButton]
    greenButton.SetButtonList buttons
    redButton.SetButtonList buttons
    yellowButton.SetButtonList buttons
    blueButton.SetButtonList buttons

    // Start Button
    let startButton = Shapes.Ellipse(Width=75.,Height=75.,Fill=SolidColorBrush(Color.FromRgb(190uy, 0uy, 0uy)), Stroke=Brushes.Black)
    c.Children.Add(startButton) |> ignore
    System.Windows.Controls.Canvas.SetLeft(startButton, width/2.-startButton.Width/2.)
    System.Windows.Controls.Canvas.SetTop(startButton, 540.-startButton.Height/2.)
    let startLabel = Controls.TextBlock(Text="START", Height=75., Width=75., FontSize=30., FontFamily=FontFamily("Digital-7 Mono"), Foreground=Brushes.Black)
    ignore <| c.Children.Add startLabel
    System.Windows.Controls.Canvas.SetLeft(startLabel, width/2.-startLabel.Width/2.+3.)
    System.Windows.Controls.Canvas.SetTop(startLabel, 540.-startLabel.Height/2.+25.)
    startButton.MouseEnter.Add(fun _ ->
        if not (manager.GameHasStarted()) then
            manager.Unlock Started
            manager.Lock HandsReset
            levelLabel.Dispatcher.Invoke(DispatcherPriority.Render, Action(fun () -> levelLabel.Text <- "01")) |> ignore
            startButton.Fill <- SolidColorBrush(Color.FromRgb(255uy, 0uy, 0uy))
            async { System.Console.Beep(1500, 400) } |> Async.Start
            async { 
                // sleep for pulse ms so light appears to flash
                Thread.Sleep(lightPulse) 
                startButton.Dispatcher.Invoke(fun _ -> startButton.Fill <- SolidColorBrush(Color.FromRgb(190uy, 0uy, 0uy)))
            } |> Async.Start
            async {
                Thread.Sleep(500)
                Thread.Sleep(200)
                startGame buttons
            } |> Async.Start
    )
    // End Build Game

    // Hands
    let rh = Shapes.Path(Fill=Brushes.Orange, Stroke=Brushes.Black, Data=EllipseGeometry(Point(0.0,0.0), 9., 9.))
    let lh = Shapes.Path(Fill=Brushes.Orange, Stroke=Brushes.Black, Data=EllipseGeometry(Point(0.0,0.0), 9., 9.))
    c.Children.Add(lh) |> ignore
    c.Children.Add(rh) |> ignore

    // Add grid to window
    w.Content <- g

    // stop sensor when window closes
    w.Unloaded.Add(fun args -> sensor.Stop())

    // react to sensor input
    let onSkeletonReady (sk : Skeleton) =
        moveMouse(sk)
        draw (sk.Joints.Item(JointType.HandRight)) rh
        draw (sk.Joints.Item(JointType.HandLeft)) lh

    // start listening to hardware for events
    skeletonProcessor sensor onSkeletonReady |> Async.Start

    // start running the app
    let a = System.Windows.Application()
    ignore <| a.Run(w)